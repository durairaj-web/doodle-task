import { BrowserRouter } from "react-router-dom";
import { Provider } from 'react-redux'

/* import :: Store Config */
import store from "./store/configureStore";

/* import :: Layout */
import { Layout } from "./common/layout";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Layout />
      </BrowserRouter>
    </Provider>
    
  );
}

export default App;
