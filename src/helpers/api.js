/* Home :: Get Content */
const URL_Register = `/api/register`;
const URL_Login = `/api/login`;

export {
    URL_Register,
    URL_Login
}