/* React Toast */
import { toast } from 'react-toastify';

/* Check if values in valid */
export const validateOptions = (value) => {
  if (value !== null &&
    value !== undefined &&
    value !== ""
  ) {
    return true;
  }

  return false;
}

export const isUserAuthenticated = (token) => {
  if (localStorage.getItem(token))
    return true;
  else
    return false;
}

export const createMarkup = (text) => {
  return { __html: text };
}

export const showToast = (toastType, toastMessage, isAutoClose) => {

  let toastOptions = {
    position: "top-right",
    autoClose: isAutoClose ? toastType === "error" ? 10000 : 5000 : false,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };

  if (toastType === "info") {
    toast.info(<div dangerouslySetInnerHTML={createMarkup(toastMessage)} />, toastOptions);
  }

  if (toastType === "success") {
    toast.success(<div dangerouslySetInnerHTML={createMarkup(toastMessage)} />, toastOptions);
  }

  if (toastType === "error") {
    toast.error(<div dangerouslySetInnerHTML={createMarkup(toastMessage)} />, toastOptions);
  }

  if (toastType === "warn") {
    toast.warn(<div dangerouslySetInnerHTML={createMarkup(toastMessage)} />, toastOptions);
  }

  if (toastType === "dark") {
    toast.dark(<div dangerouslySetInnerHTML={createMarkup(toastMessage)} />, toastOptions);
  }

  if (toastType === "default") {
    toast(<div dangerouslySetInnerHTML={createMarkup(toastMessage)} />, toastOptions);
  }

}

export const getUserDetails = (token) => {
  if (localStorage.getItem(token))
    return localStorage.getItem(token);
  else
    return false;
}