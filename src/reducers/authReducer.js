import {
    GET_USER_DATA
} from '../constants/actionTypes';

let initialState = {
    userData: []
}

export function authReducer(state = initialState, action) {
    switch (action.type) {

        case GET_USER_DATA:
            return { ...state, userData: action.payload }

        default:
            return state;
    }
}