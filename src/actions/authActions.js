import axios from 'axios';
import { config } from "../config/config.js";

/* Import :: Action Types */
import {
    GET_USER_DATA
} from '../constants/actionTypes';

/* Import :: Api Url */
import {
    URL_Register,
    URL_Login
} from '../helpers/api';

const Register = (param) => {
  return async (dispatch) => {
    try {
      const apiResponse = await axios({
        method: 'POST',
        baseURL: config.baseURL,
        url: URL_Register,
        headers: await config.headers,
		data: param
      }).then(responseJson => {
        return responseJson;
      }).catch((error) => {
		console.log(error);
      });

      return apiResponse;
    } catch (error) {
	  console.log(error);
    }
  }
}

const captureGetAuthToken = (responseJson) => {
  return {
    type: GET_USER_DATA,
    payload: responseJson
  }
}

const Login = (param) => {
  return async (dispatch) => {
    try {
      const apiResponse = await axios({
        method: 'POST',
        baseURL: config.baseURL,
        url: URL_Login,
        headers: await config.headers,
		data: param
      }).then( async (responseJson) => {
		dispatch(captureGetAuthToken(responseJson['data']['data']['user_data']));
		await localStorage.setItem('isLoggedIn', true);
		await localStorage.setItem('userData', JSON.stringify(responseJson['data']['data']['user_data']));
        return responseJson;
      }).catch((error) => {
		console.log(error);
      });

      return apiResponse;
    } catch (error) {
		console.log(error);
    }
  }
}

export {
    Register,
    Login
}