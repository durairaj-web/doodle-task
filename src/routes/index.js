import {routes as auth} from './auth';
import {routes as home} from './home';

const authRoutes = [
    ...auth,
    ...home
]

export {
    authRoutes
}