import Loadable from "react-loadable";
import Loading from "../common/Loading";

const Login = Loadable({
    loader: () => import("../views/auth/Login"),
    loading: Loading
})

const SignUp = Loadable({
    loader: () => import("../views/auth/SignUp"),
    loading: Loading
})

export const routes = [
    {
        path: "/",
        exact: true,
        component: Login,
        name: "Login"
    },
	{
        path: "/signup",
        exact: true,
        component: SignUp,
        name: "SignUp"
    }
]