import Loadable from "react-loadable";
import Loading from "../common/Loading";

const Dashboard = Loadable({
    loader: () => import("../views/dashboard"),
    loading: Loading
})

export const routes = [
    {
        path: "/dashboard",
        exact: true,
        component: Dashboard,
        name: "Dashboard"
    }
]