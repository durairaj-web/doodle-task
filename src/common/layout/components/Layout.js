import React, { Component, Fragment } from "react";
import { Route, withRouter } from "react-router-dom";
import { connect } from 'react-redux';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

/* common section */
import Topbar from "./Topbar";

/* Imports :: Routes */
import { authRoutes } from "../../../routes";

/* Helpers */
import { isUserAuthenticated } from '../../../helpers';

const publicRoutes = [
  "Login",
  "SignUp"
];

class Layout extends Component {

  componentDidMount() {
    if (this.props.location.pathname !== "/signup") {
      const isLoggedIn = isUserAuthenticated('isLoggedIn');
      if(isLoggedIn){
        this.props.history.push("/dashboard");
      }else{
        this.props.history.push("/");
      }
    }
  }

  render() {
    return (<React.Fragment>
      <ToastContainer />

      {
        authRoutes.map((route, idx) => {
          return route.component ? (
            <Route
              key={idx}
              path={route.path}
              exact={route.exact}
              name={route.name}
              render={props => (<Fragment>
                {
                  publicRoutes.includes(route.name) ? (
                    <div className="authView">
                      <route.component {...props} />
                    </div>
                  ) : (
                    <div className="mainView">
                      <Topbar />
                      <div id="main" role="main">
                        <route.component {...props} />
                      </div>
                    </div>
                  )
                }
              </Fragment>)}
            />
          ) : null
        })
      }
    </React.Fragment>);
  }
}

const mapStateToProps = (state) => {
  return {
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout));