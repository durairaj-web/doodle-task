import "./topbar.css";
import { ExitToApp } from "@material-ui/icons";
import { Link } from "react-router-dom";

export default function Topbar() {
	
  const removeLocalStorage = async () => {
    await localStorage.clear();
    window.location.href = '/login';
  }

  return (
    <div className="topbarContainer">
      <div className="topbarLeft">
        <Link to="/" style={{ textDecoration: "none" }}>
          <span className="logo">Doodle Blue</span>
        </Link>
      </div>
      <div className="topbarCenter">
      </div>
      <div className="topbarRight">
        <div className="topbarIcons">
          <div className="topbarIconItem">
            <ExitToApp onClick={() => removeLocalStorage()} />
          </div>
        </div>
        <Link to={`#/`}>
          <img
            src="https://images.pexels.com/photos/3686769/pexels-photo-3686769.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
            alt=""
            className="topbarImg"
          />
        </Link>
      </div>
    </div>
  );
}
