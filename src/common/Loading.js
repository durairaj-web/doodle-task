const Loading = (props) => {
  if (props.error) {
    return (
      <p>error!</p>
    )
  } else if (props.pastDelay) {
    return (
      <p>loading...</p>
    );
  } else {
    return null;
  }
}

export default Loading;