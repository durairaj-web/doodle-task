import React from 'react';
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

/* Material UI */
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  spinnerContainer: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    background: '#f8f8f8ad'
  },
  spinner: {
    top: '47%',
    left: '47%',
    zIndex: 1000,
    position: 'absolute'
  }
}));

const PageLoader = (props) => {
  const classes = useStyles();
  const { loading } = props;

  return (<>
    {
      loading && <Grid container className={classes.spinnerContainer}>
        <Loader
          type="TailSpin"
          color="#00BFFF"
          height={100}
          width={100}
          timeout={0} //3 secs
          className={classes.spinner}
        />
      </Grid>
    }
  </>); 
}

export default PageLoader;



