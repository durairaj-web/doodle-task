const getHeaders = async () => {
  // let authResp = await getHeaderToken();
  let headers = {
    // 'Authorization': `Bearer ${authResp.data.access_token}`,
    'Content-Type': 'application/json'
  }

  return headers;
}

const siteConfig = {
    baseURL: 'http://localhost:9999/',
    headers: getHeaders()
}

export const config = siteConfig;