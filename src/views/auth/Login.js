import React, { useState } from 'react';
import { connect } from 'react-redux';

/* import material ui components */
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';

/* import sub components */
import Copyright from './components/Copyright';
import PageLoader from '../../common/loader/PageLoader';

/* Imports :: Helpers */
import { validateOptions, showToast } from '../../helpers';

/* Redux :: Actions */
import * as authActions from '../../actions';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  alerts: {
    marginTop: theme.spacing(2),
    width: '100%'
  }
}));

function Login(props) {
  const classes = useStyles();
  const [formValues, setFormValues] = useState({email: '', password : ''});
  const [errorMsg, setErrorMsg] = useState('');
  const [formSubmit, setFormSubmit] = useState(false);
  
  const validateLogin = async (event) => {
    event.preventDefault();

    let formValid = true;
    if(!validateOptions(formValues.email) || !validateOptions(formValues.password)){
      formValid = false;
    }

    if(formValid){
      setFormSubmit(true);
      await props.Login({
        email: formValues.email,
        password: formValues.password
      }).then(result => {
        if(validateOptions(result) && result['data']['status'] === "ok"){
          showToast('success','Login successful', true);
          props.history.push('/dashboard');
        }else{
          setErrorMsg('Incorrect username or password');
          setFormSubmit(false);
        }
      })
    }else{
      setErrorMsg('Incorrect username or password');
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        { validateOptions(errorMsg) && <Alert variant="filled" severity="error" className={classes.alerts}>{errorMsg}</Alert> }
        <form className={classes.form} noValidate onSubmit={validateLogin}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="off"
            autoFocus
            onChange={(e) => {
              const formValue = { ...formValues };
              formValue.email = e.target.value;
              setFormValues(formValue);
            }}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="off"
            onChange={(e) => {
              const formValue = { ...formValues };
              formValue.password = e.target.value;
              setFormValues(formValue);
            }}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >Sign In</Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/signup" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>

      <PageLoader loading={formSubmit} />
    </Container>
  );
}
  
const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    Login: (param) => { return dispatch(authActions.Login(param)).then(result => { return result; }) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);