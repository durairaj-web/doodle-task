import React, { useState } from 'react';
import { connect } from 'react-redux';

/* import material ui components */
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

/* import sub components */
import Copyright from './components/Copyright';
import PageLoader from '../../common/loader/PageLoader';

/* Imports :: Helpers */
import { validateOptions, showToast } from '../../helpers';

/* Redux :: Actions */
import * as authActions from '../../actions';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function SignUp(props) {
  const classes = useStyles();
  const [formValues, setFormValues] = useState({email: '', password : '', cpassword: ''});
  const [errors, setErros] = useState({email: '', password: '', cpassword : ''});
  const [formSubmit, setFormSubmit] = useState(false);
  
  const saveFormValues = (fieldName, value) => {
    const formValue = { ...formValues };
    formValue[fieldName] = value.trim();
    setFormValues(formValue);
  }
  
  const validateForm = () => {
    const error = { ...errors };
    error.email = '';
    error.password = '';
    error.cpassword = '';   
    let formValid = true;
    if(!validateOptions(formValues.email)){
      error.email = 'This field is required.';
      formValid = false;
    }
    if(validateOptions(formValues.email)){
      var mailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      if(!formValues.email.match(mailFormat)){
        error.email = 'Invalid email address';
        formValid = false;
      }
    }
    if(!validateOptions(formValues.password)){
      error.password = 'This field is required.';
      formValid = false;
    }
    if(validateOptions(formValues.password)){
      var pwdFormat = /^(?=.*[a-z])(?=.*[A-Z]).{6,15}$/;
      if(!formValues.password.match(pwdFormat)){
        error.password = 'Password should be 6 to 20 characters which contain at least one uppercase and one lowercase letter';
        formValid = false;
      }
    }
    if(!validateOptions(formValues.cpassword)){
      error.cpassword = 'This field is required.';
      formValid = false;
    }
    if(formValues.password !== formValues.cpassword){
		console.log(formValues.password);
		console.log(formValues.cpassword);
      error.cpassword = 'Confirm password not matching.';
      formValid = false;
    }
    setErros(error);    
    return formValid;
  }
  
  const validateSignup = async (event) => {
    event.preventDefault();

    if(validateForm()){
      setFormSubmit(true);
      await props.Register({
        email: formValues.email,
        password: formValues.password
      }).then(result => {
        if(result['data']['status'] === "ok"){
          showToast('success','You have successfully registered.', true);
          props.history.push('/');
        }else{
          showToast('error',result['data']['error'], true);
          setFormSubmit(false);
        }
      })
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate onSubmit={validateSignup}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="off"
                onChange={ async (e) => {
                  await saveFormValues('email', e.target.value)
                  await validateForm();
                }}
                error={validateOptions(errors.email)}
                helperText={errors.email}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="off"
                onChange={ async (e) => {
                  await saveFormValues('password', e.target.value);
                  await validateForm();
                }}
                error={validateOptions(errors.password)}
                helperText={errors.password}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="cpassword"
                label="Confirm Password"
                type="password"
                id="cpassword"
                autoComplete="off"
                onChange={ async (e) => {
                  await saveFormValues('cpassword', e.target.value)
                  await validateForm();
                }}
				onBlur={ async () => {
                  await validateForm();
                }}
                error={validateOptions(errors.cpassword)}
                helperText={errors.cpassword}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >Sign Up</Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>

      <PageLoader loading={formSubmit} />
    </Container>
  );
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    Register: (param) => { return dispatch(authActions.Register(param)).then(result => { return result; }) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);