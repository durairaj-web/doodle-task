import { useEffect, useRef, useState } from "react";
import axios from "axios";
import { io } from "socket.io-client";
import Select from 'react-select'

import Conversation from "./components/conversations/Conversation";
import Message from "./components/message/Message";

import "./messenger.css";

/* Imports :: Helpers */
import { getUserDetails, showToast } from '../../helpers';

import { config } from "../../config/config.js";

const Messenger = (props) => {
  const [usersList, setUsersList] = useState([]);
  const [conversations, setConversations] = useState([]);
  const [currentChat, setCurrentChat] = useState(null);
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState("");
  const [arrivalMessage, setArrivalMessage] = useState(null);
  const socket = useRef();
  const user = JSON.parse(getUserDetails('userData'));
  const scrollRef = useRef();

  useEffect(() => {
    socket.current = io("ws://localhost:8900");
    socket.current.on("getConversations", (data) => {
      setConversations(data);
    });
    socket.current.on("getMessage", (data) => {
      showToast('success','<div><h4>'+data.senderName+'</h4><p>'+data.text+'</p></div>', true);
      setArrivalMessage({
        sender: data.senderId,
        text: data.text,
        createdAt: Date.now(),
      });
    });
  }, []);

  useEffect(() => {
    arrivalMessage &&
      currentChat?.members.includes(arrivalMessage.sender) &&
      setMessages((prev) => [...prev, arrivalMessage]);
  }, [arrivalMessage, currentChat]);

  useEffect(() => {
    socket.current.emit("addUser", user._id);
  }, [user]);

  useEffect(() => {
    const getConversations = async () => {
      try {
        const res = await axios.get(config.baseURL+"api/conversations/" + user._id);
        setConversations(res.data);
      } catch (err) {
        console.log(err);
      }
    };
    getConversations();
    const getUsers = async () => {
      try {
        const res = await axios.get(config.baseURL+"api/users/");
        setUsersList(res.data);
      } catch (err) {
        console.log(err);
      }
    };
    getUsers();
  }, [user._id]);

  useEffect(() => {
    const getMessages = async () => {
      try {
        const res = await axios.get(config.baseURL+"api/messages/" + currentChat?._id);
        setMessages(res.data);
      } catch (err) {
        console.log(err);
      }
    };
    getMessages();
  }, [currentChat]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const message = {
      sender: user._id,
      text: newMessage,
      conversationId: currentChat._id,
    };

    const receiverId = currentChat.members.find(
      (member) => member !== user._id
    );

    socket.current.emit("sendMessage", {
      senderId: user._id,
      receiverId,
      text: newMessage,
    });

    try {
      const res = await axios.post(config.baseURL+"api/messages", message);
      setMessages([...messages, res.data]);
      setNewMessage("");
    } catch (err) {
      console.log(err);
    }
  };

  const fetchUser = async(selectedUser) => {
    let conversationExist = 0;
    conversations.map(x => {
      if(x.members.includes(selectedUser._id)){
        conversationExist = 1;
      }
    })

    if(conversationExist === 0){
      const conversation = {
        senderId: user._id,
        receiverId: selectedUser._id,
      };
      
      try {
        const res = await axios.post(config.baseURL+"api/conversations", conversation);
        setMessages([...messages, res.data]);
        setNewMessage("");
      } catch (err) {
        console.log(err);
      }
    }
    socket.current.emit("addConversations", user._id);
  }

  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  return (
    <>
      <div className="messenger">
        <div className="chatMenu">
          <div className="chatMenuWrapper">
            { /* <input placeholder="Search for friends" className="chatMenuInput" /> */ }
            <Select
              placeholder={'search user'}
              value={null}
              options={usersList.filter(x => user._id !== x._id )}
              onChange={(selectedOption) => {
                fetchUser(selectedOption);
              }}
              getOptionLabel={(option) => option.username}
              getOptionValue={(option) => option._id}  
            />
            {conversations.map((c) => (
              <div onClick={() => setCurrentChat(c)}>
                <Conversation conversation={c} currentUser={user} />
              </div>
            ))}
          </div>
        </div>
        <div className="chatBox">
          <div className="chatBoxWrapper">
            {currentChat ? (
              <>
                <div className="chatBoxTop">
                  {messages.map((m) => (
                    <div ref={scrollRef}>
                      <Message message={m} own={m.sender === user._id} />
                    </div>
                  ))}
                </div>
                <div className="chatBoxBottom">
                  <textarea
                    className="chatMessageInput"
                    placeholder="write something..."
                    onChange={(e) => setNewMessage(e.target.value)}
                    value={newMessage}
                  ></textarea>
                  <button className="chatSubmitButton" onClick={handleSubmit}>
                    Send
                  </button>
                </div>
              </>
            ) : (
              <span className="noConversationText">
                Open a conversation to start a chat.
              </span>
            )}
          </div>
        </div>
      </div>
    </>
  );
}

export default Messenger;