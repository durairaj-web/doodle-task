const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const User = require('./express/model/user')
const Message = require('./express/model/message')
const Conversation = require('./express/model/conversation')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const socket = require("socket.io")

const JWT_SECRET = '1zquP6zsYEEFXLC4V2lQln7JOBfKcNKo6D6294jo0xjdl9jAXymlwkvHjExLqm7v'

// mongoose connectivity
mongoose.connect('mongodb+srv://root:cd7CJ6STycKTwau5@cluster0.nccjf.mongodb.net/chat_app', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true
})

// create server
const app = express()
app.use(bodyParser.json())
app.use((req, res, next) => {
  const allowedOrigins = [
    'http://localhost:3000'
  ];
  const origin = req.headers.origin;

  if (allowedOrigins.includes(origin)) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  }

  res.header('Access-Control-Allow-Methods', 'GET, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  res.header('Access-Control-Allow-Credentials', true);
  return next();
});

const server = app.listen(9999, () => {
	console.log('Server up at 9999')
})

// Socket setup
const io = socket(8900, {
	cors: {
	  origin: "http://localhost:3000",
	},
})
io.on("connection", function (socket) {
  //take userId and socketId from user
  socket.on("addUser", (userId) => {
    addUser(userId, socket.id);
    io.emit("getUsers", users);
  });
  
  socket.on("addConversations", async (userId) => {
    let conversations = await Conversation.find({
      members: { $in: [userId] },
    });
    io.emit("getConversations", conversations);
  });

  //send and get message
  socket.on("sendMessage", async({ senderId, receiverId, text }) => {
    const userDetail = await User.findOne({_id:senderId});
    const user = getUser(receiverId);
    console.log(userDetail);
    io.to(user.socketId).emit("getMessage", {
      senderName: userDetail.username,
      senderId,
      text,
    });
  });

  //when disconnect
  socket.on("disconnect", () => {
    console.log("a user disconnected!");
    removeUser(socket.id);
    io.emit("getUsers", users);
  });
});

let users = [];
const addUser = (userId, socketId) => {
  !users.some((user) => user.userId === userId) &&
    users.push({ userId, socketId });
};

const removeUser = (socketId) => {
  users = users.filter((user) => user.socketId !== socketId);
};

const getUser = (userId) => {
  return users.find((user) => user.userId === userId);
};

app.get('/', (req, res) => {
  res.send('Server is running!')
})

// register api
app.post('/api/register', async (req, res) => {

	// input validation
	const { email, password: plainTextPassword } = req.body
	if (!email || typeof email !== 'string') {
		return res.json({ status: 'error', error: 'Email address is required' })
	}else{
		var mailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
		if(!email.match(mailFormat)){
			return res.json({ status: 'error', error: 'Invalid email address' })
		}
	}

	if (!plainTextPassword || typeof plainTextPassword !== 'string') {
		return res.json({ status: 'error', error: 'Invalid password' })
	}else{
		var pwdFormat = /^(?=.*[a-z])(?=.*[A-Z]).{6,15}$/;
		if(!plainTextPassword.match(pwdFormat)){
			return res.json({
				status: 'error',
				error: 'Password should be 6 to 15 characters which contain at least one uppercase and one lowercase letter'
			})
		}
	}

	// hash the plain password
	const password = await bcrypt.hash(plainTextPassword, 10)

	// insert the data
	try {
		const response = await User.create({
			username: email,
			password
		})
		console.log('User created successfully: ', response)
	} catch (error) {
		console.log(error);
		if (error.code === 11000) {
			// duplicate key
			return res.json({ status: 'error', error: 'Email already taken' })
		}
		throw error
	}

	res.json({ status: 'ok' })
})

// login api
app.post('/api/login', async (req, res) => {
	const { email, password } = req.body
	const user = await User.findOne({ username: email }).lean()

	if (!user) {
		return res.json({ status: 'error', error: 'Incorrect username or password' })
	}

	if (await bcrypt.compare(password, user.password)) {
		// the username, password combination is successful

		const token = jwt.sign(
			{
				id: user._id,
				username: user.username
			},
			JWT_SECRET
		)

		return res.json({ status: 'ok', data: {user_data:{_id:user._id, email, 'token':JWT_SECRET}} })
	}

	res.json({ status: 'error', error: 'Incorrect username or password' })
})

//add
app.post("/api/messages", async (req, res) => {
	const newMessage = new Message(req.body);

	try {
	  const savedMessage = await newMessage.save();
	  res.status(200).json(savedMessage);
	} catch (err) {
	  res.status(500).json(err);
	}
});

//get
app.get("/api/messages/:conversationId", async (req, res) => {
  try {
    const messages = await Message.find({
	  conversationId: req.params.conversationId,
    });
    res.status(200).json(messages);
  } catch (err) {
    res.status(500).json(err);
  }
});


//new conv
app.post("/api/conversations/", async (req, res) => {
  const newConversation = new Conversation({
    members: [req.body.senderId, req.body.receiverId],
  });

  try {
    const savedConversation = await newConversation.save();
    res.status(200).json(savedConversation);
  } catch (err) {
    res.status(500).json(err);
  }
});

//get conv of a user
app.get("/api/conversations/:userId", async (req, res) => {
  try {
    const conversation = await Conversation.find({
      members: { $in: [req.params.userId] },
    });
    res.status(200).json(conversation);
  } catch (err) {
    res.status(500).json(err);
  }
});

// get conv includes two userId
app.get("/api/conversations/find/:firstUserId/:secondUserId", async (req, res) => {
  try {
    const conversation = await Conversation.findOne({
      members: { $all: [req.params.firstUserId, req.params.secondUserId] },
    });
    res.status(200).json(conversation)
  } catch (err) {
    res.status(500).json(err);
  }
});

//get all user
app.get("/api/users", async (req, res) => {
	try {
	  const user = await User.find({},{_id:1,username:1});
	  res.status(200).json(user);
	} catch (err) {
	  res.status(500).json(err);
	}
});

//get a user
app.get("/api/user", async (req, res) => {
	const userId = req.query.userId;
	try {
	  const user = await User.findById(userId)
	  res.status(200).json(user);
	} catch (err) {
	  res.status(500).json(err);
	}
});

//add
app.post("/api/messages", async (req, res) => {
  const newMessage = new Message(req.body);

  try {
    const savedMessage = await newMessage.save();
    res.status(200).json(savedMessage);
  } catch (err) {
    res.status(500).json(err);
  }
});

//get
app.get("/api/messages/:conversationId", async (req, res) => {
  try {
    const messages = await Message.find({
      conversationId: req.params.conversationId,
    });
    res.status(200).json(messages);
  } catch (err) {
    res.status(500).json(err);
  }
});